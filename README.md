# Welcome to my personal portfolio! 👋

Here, you'll find all my public works. 
They cover topics like:

- **Data Science**: machine learning and deep learning algorithms applications. Classification, regression and vision computing resolutions. 
- **MLOps**:  MLFlow model tracking, online deployment, APIs, GUIs.
- **Data Engineering**: ETL, SQL and NoSQL databases, Airflow data pipelines, shell scripting.
- Containers orchestration
- Etc.

I hope you find my projects interesting and valuable. Please feel free to contact me if you have any questions or suggestions!

[**LinkedIn**](https://www.linkedin.com/in/natanael-ferran/) - [**E-mail**](mailto:natanaelferran@protonmail.com) - [**Telegram**](https://t.me/nata_fq)
